#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

// Builds the story based on an array
std::string getMadLib(std::string madLib[]) {
    std::string story;
    story = "Many " + madLib[0] + " photographers make big money photographing " + madLib[1] + " and beautiful " + madLib[2] + ".\n" 
        + "They sell prints to " + madLib[3] + " magazines or agencies who use them in " + madLib[4] + " advertisements.\n"
        + "To be a photographer, you have to have a " + madLib[5] + " camera.\n"
        + "You also need an " + madLib[6] + " meter and filters and a special close-up " + madLib[7] + ".\n"
        + "Then you either hire a professional " + madLib[8] + " or go out and snap good pictures of ordinary " + madLib[9] + ".\n"
        + "But if you want to have a good career, you must study very " + madLib[10] + " for at least " + madLib[11] + " years.";
    return story;
}

int main()
{
    std::string madLibFormat[12] = {"adjective","plural noun","plural noun","adjective", "noun", "noun", "adjective", "noun", "adjective (ending in -er)", "plural noun", "adverb (ending in -ly)", "number"};
    std::string madLibAnswers[12];

    std::cout << "Mad lib: Photography\n";

    for (int i = 0; i < 12; i++) {
        std::cout << "Enter a " << madLibFormat[i] << ": ";
        std::cin >> madLibAnswers[i];
    }

    std::cout << "\n\n" << getMadLib(madLibAnswers);

    std::cout << "\n\nWould you like to save this mad lib? (Y/N)";
    char input;
    std::cin >> input;

    if (input == 'Y') {

        ofstream ofs("MadLib.txt");

        ofs << getMadLib(madLibAnswers);

        ofs.close();

        std::cout << "\nSaved! Check in the project directory under \"MadLib.txt\"";
    }


    _getch();
    return 0;
}